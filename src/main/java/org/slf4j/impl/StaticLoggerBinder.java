package org.slf4j.impl;

import com.universeprojects.common.shared.log.LoggerFactory;
import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;


@SuppressWarnings("unused")
public class StaticLoggerBinder implements LoggerFactoryBinder {

    private static StaticLoggerBinder singleton = new StaticLoggerBinder();

    public static StaticLoggerBinder getSingleton() {
        return singleton;
    }

    public static final String REQUESTED_API_VERSION = "1.6";

    @Override
    public ILoggerFactory getLoggerFactory() {
        return LoggerFactory.getInstance();
    }

    @Override
    public String getLoggerFactoryClassStr() {
        return LoggerFactory.class.getCanonicalName();
    }
}
